#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QStyledItemDelegate>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Add some dummy data...
    mModel.append("Orange");
    mModel.append("Cherry");
    mModel.append("Watermelon");
    mModel.append("Apple");
    mModel.append("Grapes");

    ui->tableView->setModel(&mModel);
    ui->tableView->resizeColumnsToContents();

    // Observe item activation and clicking for deletion
    connect(ui->tableView, &QTableView::activated, this, &MainWindow::OnItemActivated);
    connect(ui->tableView, &QTableView::clicked, this, &MainWindow::OnItemActivated);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::OnItemActivated(const QModelIndex &index)
{
    if (index.isValid() && index.column() == MyTableModel::ColumnDeleteIcon)
    {
        auto model = ui->tableView->model();
        model->removeRows(index.row(), 1, index.parent());
    }
}
