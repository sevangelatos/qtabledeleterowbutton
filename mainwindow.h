#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "mytablemodel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void OnItemActivated(const QModelIndex &index);
private:
    Ui::MainWindow *ui;
    MyTableModel mModel;
};

#endif // MAINWINDOW_H
