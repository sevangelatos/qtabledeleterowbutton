#include "mytablemodel.h"
#include <QIcon>

MyTableModel::MyTableModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

QVariant MyTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (section == ColumnFruit && orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        return QString("Items");
    }
    return QVariant();
}

Qt::ItemFlags MyTableModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return QAbstractTableModel::flags(index);

    if (index.column() == ColumnFruit)
    {
        return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
    }

    return QAbstractTableModel::flags(index);
}

int MyTableModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return mData.size();
}

int MyTableModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return ColumnEnumCount;
}

QVariant MyTableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.column() == ColumnFruit && (role == Qt::DisplayRole || role == Qt::EditRole) )
    {
        return mData[index.row()];
    }
    else if (index.column() == ColumnDeleteIcon && (role == Qt::DecorationRole || role == Qt::EditRole))
    {
        return QIcon(":/icons/delete.png");
    }

    return QVariant();
}

bool MyTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role != Qt::EditRole || !index.isValid() || index.row() >= mData.size() || index.column() != ColumnFruit)
        return false;

    if (data(index, role) != value) {
        mData[index.row()] = value.toString();
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

void MyTableModel::append(const QString &str)
{
    int row = mData.size();
    beginInsertRows(QModelIndex(), row, row);
    mData.append(str);
    endInsertRows();
}

bool MyTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);

    // Shift all items by count
    for (int i = row; i + count < mData.size(); i++)
    {
        mData[i] = mData[i+count];
    }

    for (int j = 0; j < count; j++)
    {
        mData.pop_back();
    }

    endRemoveRows();
    return true;
}



